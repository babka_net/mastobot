"""Pydantic models for parsing the Mastodon.py dicts

See: https://mastodonpy.readthedocs.io/en/stable/02_return_values.html
"""

from pydantic import BaseModel, Field
from datetime import datetime
from typing import List
from enum import Enum
import html_text
from functools import cached_property

class Visibility(Enum):
    """Enum indicating the visibility of a status
    """
    PUBLIC = "public"
    UNLISTED = "unlisted"
    PRIVATE = "private"
    DIRECT = "direct"


class Reply(BaseModel):
    """A callback may return either this class or a string.
    If a string is returned, both the visibility and spoiler_text
    will be inherited from the status to which a reply is being made.
    """
    text: str
    visibility: Visibility
    spoiler_text: str


class Mention(BaseModel):
    id: int
    username: str
    acct: str
    url: str


class Account(BaseModel):
    id: int
    username: str
    acct: str
    display_name: str | None
    url: str
    avatar: str | None
    avatar_static: str | None
    bot: bool | None


class Status(BaseModel):
    id: int
    url: str | None
    account: Account | str
    in_reply_to_id: int | None
    content: str
    created_at: datetime
    sensitive: bool
    spoiler_text: str
    visibility: Visibility
    mentions: List[Account | str]

    class Config:
        keep_untouched = (cached_property,)

    @cached_property
    def text(self):
        """Access the normalized text extracted from the HTML content
        """
        return html_text.extract_text(self.content)

    def is_mention(self, account_name: str):
        """Check if an account is mentioned by this status
        """

        for mention in self.mentions:
            if type(mention) is Account and mention.acct == account_name:
                return True
            elif mention == account_name:
                return True

        return False


class NotificationType(Enum):
    MENTION = "mention"
    REBLOG = "reblog"
    FAVOURITE = "favourite"
    FOLLOW = "follow"
    POLL = "poll"
    FOLLOW_REQUEST = "follow_request"


class Notification(BaseModel):
    id: int
    type_: NotificationType = Field(alias="type")
    created_at: datetime
    account: Account
    status: Status | None


class Conversation(BaseModel):
    id: int
    unread: bool
    accounts: List[Account]
    last_status: Status
