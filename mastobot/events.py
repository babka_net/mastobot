"""Pydantic models for streaming API events.

See: https://docs.joinmastodon.org/methods/streaming/
"""
from pydantic import BaseModel
from enum import Enum
from .models import Status, Notification, Conversation
from typing import Any


class Source(Enum):
    """Where did an event come from?
    """
    UPDATE = "update"
    NOTIFICATION = "notification"
    DELETE = "delete"
    CONVERSATION = "conversation"


class UpdateEvent(BaseModel):
    """The `status` was added to the timeline of the bot.
    """
    status: Status

    def get_latest_status(self):
        return self.status


class NotificationEvent(BaseModel):
    """The bot received a notification.
    """
    notification: Notification

    def get_latest_status(self):
        return self.notification.status


class DeleteEvent(BaseModel):
    """A status was deleted.
    """
    status_id: int

    def get_latest_status(self):
        return None


class ConversationEvent(BaseModel):
    """An event occured as part of a Conversation/direct message chain.
    """
    conversation: Conversation

    def get_latest_status(self):
        return self.conversation.last_status


class Event(BaseModel):
    obj: Any
    source: Source

    def to_event(self):
        if self.source == Source.UPDATE:
            return UpdateEvent(status=self.obj)
        elif self.source == Source.NOTIFICATION:
            return NotificationEvent(notification=self.obj)
        elif self.source == Source.DELETE:
            return DeleteEvent(status_id=self.obj)
        elif self.source == Source.CONVERSATION:
            return ConversationEvent(conversation=self.obj)


EventTypes = UpdateEvent | NotificationEvent | DeleteEvent | ConversationEvent
