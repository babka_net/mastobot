from mastodon import Mastodon, StreamListener
from .ws import WebsocketListener
from typing import Callable
from .events import Event, Source, EventTypes, UpdateEvent, NotificationEvent
from .models import Reply, Status, NotificationType, Visibility
from functools import partial
from loguru import logger
import re
from pydantic import ValidationError
from enum import Enum
# related documentation: https://mastodonpy.readthedocs.io/en/stable/#

class Stream(Enum):
    USER = "user"
    PUBLIC = "public"
    LOCAL = "public:local"
    HASHTAG = "hashtag"
    LIST = "list"

class Bot:
    def __init__(self,
                 websocket_mode=False,
                 logger=logger):
        """Intiate a Mastodon bot.

        :param instance_url: (str) base URL for your Mastodon instance of choice,
            e.g. ``https://mastodon.technology``.
        :param access_token: (str) "Your access token" inside
            Preferences -> Development -> some application.
        :param websocket_mode: (bool) whether to use websockets for streaming
        """
        self._bot = None
        self._handle = ""  # will be like "bot@instance.tld"
        self._atname = ""  # will be like "@bot"
        self._callbacks = set()
        self._websocket_mode = websocket_mode
        self._on_update = self._trigger_callbacks_for(Source.UPDATE)
        self._on_notification = self._trigger_callbacks_for(Source.NOTIFICATION)
        self._on_delete = self._trigger_callbacks_for(Source.DELETE)
        self._logger = logger
        self._streams = dict()

    def _trigger_callbacks_for(self, source: Source):
        """Handle events from streams
        """
        def trigger(obj):

            try:
                event = Event(obj=obj, source=source).to_event()
                self._logger.debug(f"Received event {event}")
            except ValidationError as e:
                self._logger.error(f"Received an event that could not be parsed {obj}")
                raise e

            self._logger.info(f"Received event of type {source}")

            for (predicate, callback) in self._callbacks:
                self._logger.info(f"Testing {predicate} to run {callback}")

                if predicate(event):

                    self._logger.info(f"Calling {callback.__name__} for {event}")

                    result = callback(event)
                    status = event.get_latest_status()

                    if result is None:
                        continue
                    elif type(result) is str:
                        reply = Reply(text=result,
                                      visibility=status.visibility,
                                      spoiler_text=status.spoiler_text)
                    else:
                        reply = result

                    self._logger.debug(f"{callback.__name__} responds to {status.id} with {reply}")
                    self.reply(status, reply)

        return trigger

    def boost(self, status_id: int):
        """
        :param status_id: ID of the status to boost
        """
        self._logger.debug(f"Boost {status_id}")
        return self._bot.status_reblog(status_id)

    def favourite(self, status_id: int):
        """
        :param status_id: ID of the status to favourite
        """
        self._logger.debug(f"Favourite {status_id}")
        return self._bot.status_favourite(status_id)

    def like(self, status_id: int):
        """Alias for favourite
        """
        return self.favorite(status_id)

    def reply(self, status: Status, reply: Reply):
        """Reply to a status

        :param status: Status to reply to
        :param reply: Reply to make.
        """
        self._logger.debug(f"Reply to {status.id} with {reply}")
        self._bot.status_post(in_reply_to_id=status.id,
                              status=reply.text,
                              visibility=reply.visibility.value,
                              spoiler_text=reply.spoiler_text)

    def post(self,
             text: str,
             visibility: Visibility = Visibility.PUBLIC,
             spoiler_text: str | None = None):
        """Post a status. Use reply if you want to reply to a status.

        :param text: Text of the status
        :param visibility: Visibility of the status.
        :param spoiler_text: Spoiler text/summary.
        """
        return Status.parse_obj(
            self._bot.status_post(status=text,
                                  visibility=visibility.value,
                                  spoiler_text=spoiler_text))

    # predicates

    def _on_text_predicate(self, text: str):

        def predicate(event):
            status = event.get_latest_status()
            if status is None:
                return False

            return text in status.text

        return predicate

    def _on_regex_predicate(self, pattern: str):
        compiled = re.compile(pattern)

        def predicate(event):
            status = event.get_latest_status()
            if status is None:
                return False

            return compiled.match(status.text)

        return predicate

    # decorator generators

    def register_decorator(self, predicate: Callable[[EventTypes], bool],
                           name: str | None = None):
        """Register a predicate as a decorator for reuse.
        Example:
        ```python
        def on_help(event):
            return type(event) is NotificationEvent and event.get_latest_status().text.find("help") != -1

        bot.register_decorator(on_help)

        @bot.on_help()
        ```

        :param predicate: the predicate function to register as a decorator
        :param name: provide this to override the name underwhich the decorator will be registered. Must  be provided when the predicate is a lambda. Otherwise it is optional.
        """
        def decorator(*args, **kwargs):
            return self.on(partial(predicate, *args, **kwargs))

        decorator_name = predicate.__name__ if predicate.__name__ != "<lambda>" else name

        if decorator_name is None:
            raise ValueError("name is a required argument when registering a lambda function as a decorator")

        setattr(self, "on_" + decorator_name if not decorator_name.startswith("on_") else decorator_name, decorator)

    def on(self, predicate: Callable[[EventTypes], bool]):
        """Generic decorator for listening to all events
        :param predicate: A function taking an event and returning a bool. If it returns true, the callback will be executed.
        """

        def decorator(callback: Callable[[EventTypes], str | Reply]):
            self._callbacks.add((predicate, callback))
        return decorator

    def on_text(self, text: str):
        return self.on(self._on_text_predicate(text))

    def on_regex(self, pattern: str):
        return self.on(self._on_regex_predicate(pattern))

    def on_mention(self,
                   predicate: Callable[[EventTypes], bool] | None = None):
        """Listen to mentions"""

        def fn(event):
            if type(event) is not NotificationEvent:
                return False

            type_ = event.notification.type_
            return type_ == NotificationType.MENTION and (predicate(event) if predicate else True)

        return self.on(fn)

    def on_mention_text(self, text: str):
        return self.on_mention(self._on_text_predicate(text))

    def on_mention_regex(self, pattern: str):
        return self.on_mention((self._on_regex_predicate(pattern)))

    def on_home_update(self,
                       predicate: Callable[[EventTypes], bool] | None = None):
        """Listen to updates on the home timeline and invoke a callback with
            a ``Status`` object as argument.
        """
        return self.on(lambda event:
                       type(event) is UpdateEvent
                       and (predicate(event) if predicate else True))

    def on_home_update_text(self, text: str):
        return self.on_home_update(self._on_text_predicate(text))

    def on_home_update_regex(self, pattern: str):
        return self.on_home_update(self._on_regex_predicate(pattern))

    # execution
    def add_stream(self, stream: Stream, *args, **kwargs):
            """Add a stream to listen to.

            If the stream requires additional parameters (e.g. a hashtag for the Stream.HASHTAG) these can be provided as positional or keyword arguments to add_stream and will be forwarded to Mastodon.py.

            If there are multiple streams, all but the last stream will be run async. The last stream will be run async if run is called with run_async=True.

            [See Mastodon.py](https://mastodonpy.readthedocs.io/en/stable/10_streaming.html) for possible parameters.
            :param stream: Stream to listen to.
            :param *args: positional arguments to supply to this stream in addition to the listener.
            :param **kwargs: keyword-arguments to supply to this stream in addition to the listener.
            """

            if self._websocket_mode:
                self._logger.info(f"Using websocket mode to connect to Stream.{stream.name}")
                listener = WebsocketListener(
                    self._bot.instance(), self._token, stream=stream.value
                )
            else:
                self._logger.info(f"Using HTTP mode to connect to Stream.{stream.name}")
                listener = StreamListener()

            listener.on_update = self._on_update
            listener.on_notification = self._on_notification
            listener.on_delete = self._on_delete
            self._streams[stream.value] = (listener, args, kwargs)

    def connect(self, instance, token):
        """Establish a connection with the Mastodon instance.
        Must be called before any of the utility functions (boost, favourite, reply, post) can be called.

        Will be called by run if it has not been called before.

        """
        self._bot = Mastodon(api_base_url=instance,
                             access_token=token)

        self._instance = instance
        self._token = token

        me = self._bot.account_verify_credentials()
        self._handle = me["acct"]
        self._atname = "@" + me["username"]

        self._logger.info(f"Connected to {instance} as {self._handle}")

    def run(self, run_async=False):
        """Start bot after all callbacks were registered.

        :param run_async: If there are multiple streams, all but the last stream will be run async. This decides whether the last stream listener should run async or not.

        """
        if not self._bot:
            self.connect()

        if Stream.USER.value not in self._streams:
            self.add_stream(Stream.USER)

        multiple_streams = len(self._streams) > 1
        for i, stream in enumerate(self._streams):

            (listener, args, kwargs) = self._streams[stream]
            stream = Stream(stream)
            self._logger.info(f"Start to listen for {stream.name}")

            if self._websocket_mode:
                listener.start_stream()
            else:
                args = list(args)
                args.append(listener)
                kwargs["run_async"] = kwargs["run_async"] or run_async or (multiple_streams and len(self._streams) != i + 1)

                match stream:
                    case Stream.USER:
                        self._bot.stream_user(*args, **kwargs)
                    case Stream.PUBLIC:
                        self._bot.stream_public(*args, **kwargs)
                    case Stream.LOCAL:
                        self._bot.stream_local(*args, **kwargs)
                    case Stream.LIST:
                        self._bot.stream_list(*args, **kwargs)
                    case Stream.HASHTAG:
                        self._bot.stream_hashtag(*args, **kwargs)
