# Hacking Mastobot

This document assumes you have read the README.

### What is Mastobot?
Mastobot is a wrapper around Mastodon.py providing a nice interface for developers to create their own bots and listened for events.

### How does Mastobot work?
Mastobot accomplishes three tasks:
1. Serialization of Mastodon.py dictionaries into Pydantic models (see `data_types.py` and `events.py`)
2. Creating callbacks for certain Mastodon events using generic, predicate-based decorators.
3. Provide a layer of abstraction over Mastodon.py

When an event is received by Mastobot from Mastodon.py, the dictionaries are always converted into a Pydantic model. These parsed events are then passed to a set of predicate functions and if they return true, the associated callback function is executed.

All callbacks are registered using the generic `on` decorator, which takes a predicate and a callback and adds them to the above mentioned set. Any other callback, whether defined in `mastobot` or registered by the developer, is just calling `on` under the hood with a specific predicate.

A Mastobot instance also provides developers with a set of functions that abstract the existing Mastodon.py interface, such as `Bot.boost`, `Bot.favourite`, `Bot.reply` and `Bot.post`.

### Contributing
If you have found a bug, please file an issue [here](https://gitlab.com/babka_net/mastobot/-/issues).

If you want to contribute to the development or want to upstream a predicate you have created, please create a [merge request](https://gitlab.com/babka_net/mastobot/-/merge_requests).
