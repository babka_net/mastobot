from mastobot.events import UpdateEvent, Event, Source
from .sample_dicts import status, account


def test_update_event():

    event = Event(obj=status, source=Source.UPDATE).to_event()

    assert type(event) is UpdateEvent
    assert event.status.id == status["id"]
    assert event.status.url == status["url"]
    assert event.status.account.id == account["id"]

