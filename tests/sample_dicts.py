
account = {
    "id": 1,
    "username": "example",
    "acct": "example@example.com",
    "display_name": "Example",
    "url": "https://example.com/@example",
    "avatar": "https://example.com/avatar/@example",
    "avatar_static": "https://example.com/avatar-static/@example",
    "bot": False
}

status = {
    "id": 1,
    "uri" : "tag:example.com,2016-11-25:objectId=<id>:objectType=Status",
    "url" : "https://example.com",
    "account": account,
    "in_reply_to_id": None,
    "in_reply_to_account_id": None,
    "reblog": False,
    "content": "<p>Hello, World</p>",
    "created_at": "2023-03-21T11:58:27.112098",
    "reblogs_count": 5,
    "favourites_count": 5,
    "reblogged": False,
    "favourited": False,
    "sensitive": False,
    "spoiler_text": "Warning",
    "visibility": "public",
    "mentions" : [account],
    "media_attachments": [],
    "emojis": [],
    "tags": [],
    "bookmarked": False,
    "application": None,
    "language": "en",
    "muted": False,
    "pinned": False,
    "replies_count": 51,
    "card": None,
    "poll": None
}

notification = {
        "id": 123,
        "type": "mention",
        "created_at": "2023-03-21T11:58:27.112098",
        "account": account,
        "status": status
}
