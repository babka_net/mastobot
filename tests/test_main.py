from mastodon import Mastodon
from mastobot import Bot
from mastobot.events import UpdateEvent, NotificationEvent
from mastobot.models import Status, NotificationType
from .sample_dicts import status, notification
import pytest

@pytest.fixture(autouse=True)
def mastodonpy(monkeypatch):
    monkeypatch.setattr(Mastodon, "status_post", lambda *args, **kwargs: None)
    monkeypatch.setattr(Mastodon, "account_verify_credentials",
                        lambda *args, **kwargs: {
                                    "acct": "bob@example.com",
                                    "username": "bob"
                        })


@pytest.fixture
def bot(monkeypatch):
    bot = Bot()
    return bot


def test_create_bot(bot):
    assert not bot._websocket_mode


def test_connect_bot(bot):
    bot.connect("example.com", "ABC")

    assert bot._handle == "bob@example.com"
    assert bot._atname == "@bob"


def test_trigger_callbacks_for_update(bot):

    update_event_called = False

    @bot.on(lambda event: type(event) is UpdateEvent)
    def update_event(event):
        nonlocal update_event_called
        update_event_called = True

        assert event.status == Status.parse_obj(status)

    bot._on_update(status)

    assert update_event_called


def test_on_mention(bot):

    was_called = False

    @bot.on_mention()
    def mention(event):
        nonlocal was_called
        was_called = True

        assert event.get_latest_status().is_mention("example@example.com")
        assert type(event) is NotificationEvent
        assert event.notification.type_ == NotificationType.MENTION

    bot._on_notification(notification)

    assert was_called


def test_on_home_update(bot):

    was_called = False

    @bot.on_home_update()
    def home_update(event):
        nonlocal was_called
        was_called = True

        assert event.status

    bot._on_update(status)
    assert was_called


def test_on_text(bot):
    was_called = False

    pattern_status = status
    pattern_status["content"] = "<p>example.com</p>"

    @bot.on_text("example.com")
    def on_text(event):
        nonlocal was_called
        was_called = True

    bot._on_update(pattern_status)
    assert was_called


def test_on_regex(bot):
    was_called = False

    pattern_status = status
    pattern_status["content"] = "<p> 123-456-789</p>"

    @bot.on_regex("([0-9]{3}-){2}[0-9]{3}")
    def on_regex(event):
        nonlocal was_called
        was_called = True

    bot._on_update(pattern_status)
    assert was_called
