from mastobot import Bot, Stream
from mastobot.events import NotificationEvent
from os import environ

bot = Bot(websocket_mode=False)


# register_decorator

def on_command(command, event):
    return (type(event) is NotificationEvent and command in event.get_latest_status().text)


bot.register_decorator(on_command)


@bot.on_command("/help")
def get_help(event):
    return """Hi, I'm an example bot.
    I can greet you with /greet.
    """


@bot.on_command("/greet")
def get_greeting(event):
    return f"""Welcome to {environ['INSTANCE']}\nFor more info mention me with /help in the message."""


# add_stream

seen = set()


def on_local(event):
    status = event.get_latest_status()
    if not status:
        return False

    if status.id in seen or status.account.acct == bot._handle:
        return False

    seen.add(status.id)
    return "@" not in status.account.acct


bot.register_decorator(on_local)


# prints the sanitized content of all posts on the local timeline.
@bot.on_local()
def print_local(event):
    status = event.get_latest_status()
    print(status.account.username, status.text)

bot.add_stream(Stream.LOCAL)

bot.connect(environ.get("INSTANCE"),
            environ.get("ACCESS_TOKEN"))
bot.run(run_async=True)

while True:
    post = input("Toot>")
    bot.post(post + '👁️')
