from mastobot import Bot
from os import environ


bot = Bot(websocket_mode=False)


@bot.on_mention(lambda event: "/help" in event.get_latest_status().text)
def get_help(event):
    return """Hi, I'm an example bot.
    I can greet you with /greet.
    """


@bot.on_mention_text("/greet")
def get_greeting(event):
    return f"""Welcome to {environ['INSTANCE']}\nFor more info mention me with /help in the message."""


bot.connect(environ.get("INSTANCE"),
            environ.get("ACCESS_TOKEN"))
bot.run()
