### 2023-03-29
#### Added
- `on_mention` and `on_home_update` now expect a predicate functions instead of strings
- Generic `on` decorator allows for predicates based on all events received by the bot.
- Logging with `loguru` (other logging options can be supplied through `logger` to the Bot's constructor)
- Serialization of Mastodon.py dictionaries using pydantic
- Support for `ConversationEvent`
- Poetry for building and dependency management

#### Remove
- `trigger.py`, `structs.py` and `constants.py` have been replaced by `data_types.py` and `events.py`.
